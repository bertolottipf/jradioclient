package com.bpfking.pyradioclient;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class pyRadioClent {

	private JFrame frame;
	int volume = 88;
	int canale = 1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pyRadioClent window = new pyRadioClent();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	Client client;
	
	/**
	 * Create the application.
	 */
	public pyRadioClent() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		client = new Client("localhost", 8000);
				
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 284, 251);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnNewButton = new JButton("+");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				volume++;
				String r = null;
				try {
					r = client.send("mpc volume " + volume + "\n");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println(r);
			}
		});
		frame.getContentPane().add(btnNewButton, BorderLayout.NORTH);
		
		JButton btnNewButton_1 = new JButton("<");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				canale--;
				String r = null;
				try {
					r = client.send("mpc play " + canale);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println(r);
			}
		});
		frame.getContentPane().add(btnNewButton_1, BorderLayout.WEST);
		
		JButton btnNewButton_2 = new JButton(">");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				canale++;
				//int r = client.send("mpc play " + canale);
				String r = null;
				try {
					r = client.send("mpc play " + canale);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println(r);
			}
		});
		frame.getContentPane().add(btnNewButton_2, BorderLayout.EAST);
		
		JButton btnNewButton_3 = new JButton("-");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				volume--;
				String r = null;
				try {
					r = client.send("mpc volume " + volume);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println(r);
			}
		});
		frame.getContentPane().add(btnNewButton_3, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnNewButton_4 = new JButton("1");
		panel.add(btnNewButton_4);
		
		JButton btnNewButton_7 = new JButton("2");
		panel.add(btnNewButton_7);
		
		JButton btnNewButton_5 = new JButton("3");
		panel.add(btnNewButton_5);
		
		JButton btnNewButton_10 = new JButton("4");
		panel.add(btnNewButton_10);
		
		JButton btnNewButton_6 = new JButton("5");
		panel.add(btnNewButton_6);
		
		JButton btnNewButton_8 = new JButton("6");
		panel.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("7");
		panel.add(btnNewButton_9);
		
		JButton btnNewButton_11 = new JButton("8");
		panel.add(btnNewButton_11);
		
		JButton button = new JButton("9");
		panel.add(button);
	}

}
