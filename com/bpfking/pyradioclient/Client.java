package com.bpfking.pyradioclient;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;



public class Client {

	private Socket clientSocket = null;
	protected String serverResponse;

	public Client(String ip, int porta) {

		try {
			// open a socket connection
			clientSocket = new Socket(ip, porta);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String send(String string) throws IOException {
		String sentence = string;
		String serverResponse;
		
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		
		outToServer.writeBytes(sentence + '\n');
		outToServer.flush();
		
		while( (serverResponse=inFromServer.readLine()) != null ) {

		    // prints the html correctly, hooray!!
		    System.out.println(serverResponse);
		}
		/*
		serverResponse = inFromServer.readLine();
		System.out.println("FROM SERVER: " + serverResponse);*/
		
		//clientSocket.close();
		
		
		return serverResponse;
	}
	
	
}
